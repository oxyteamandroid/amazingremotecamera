/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  Elf/AmazingRemoteCamera project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.remotecamera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.RightScrollView;

public class LookPictureActivity extends RightScrollActivity {
    // 右滑
    private RightScrollView mView;
    private ImageView displayImageView;
    private ImageView exitImageView;
    private Bitmap mBitmap;
    private boolean mScreenisRound = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent != null) {
            byte[] bis = intent.getByteArrayExtra("bitmap");
            mBitmap = BitmapFactory.decodeByteArray(bis, 0, bis.length);
        }

        // 右滑需要语句，初始不允许右滑
        mView = getRightScrollView();

        mView.disableRightScroll();
        mScreenisRound = Utils.isCircularScreen();
        if (mScreenisRound) {
            mView.setContentView(R.layout.dialog_look_picture_round);
            mView.setBackgroundResource(R.drawable.round);
        } else {
            mView.setContentView(R.layout.dialog_look_picture);
        }
        displayImageView = (ImageView) findViewById(R.id.dialog_display_imageview);
        exitImageView = (ImageView) findViewById(R.id.dialog_exit_imageview);
        exitImageView.setOnClickListener(onClickListener);
        exitImageView.setClickable(false);

        show();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (mView != null) {
            mView.enableRightScroll();
        }
        exitImageView.setClickable(true);
    }

    private void show() {
        if (mBitmap != null && !mBitmap.isRecycled()
                && displayImageView != null) {
            displayImageView.setImageBitmap(mBitmap);
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        if (mBitmap != null && !mBitmap.isRecycled()) {
            mBitmap.recycle();
            mBitmap = null;
        }
        super.onDestroy();
    }

    private OnClickListener onClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            exitImageView.setClickable(false);
            mView.disableRightScroll();
            finish();
        }
    };

}
