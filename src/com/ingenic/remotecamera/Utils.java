package com.ingenic.remotecamera;

import com.ingenic.iwds.HardwareList;

public class Utils {
    /**
     * 判断当前的屏是圆还是方形
     * @return
     */
    public static boolean isCircularScreen() {
        return HardwareList.IsCircularScreen();
    }
}
