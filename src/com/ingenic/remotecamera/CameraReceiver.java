/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  Elf/AmazingRemoteCamera project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.remotecamera;

import com.ingenic.iwds.uniconnect.ConnectionServiceManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class CameraReceiver extends BroadcastReceiver {
    private final String spNameString = "camera";
    private final String keyName = "isLink";
    private SharedPreferences sp;
    private Editor editor;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        final String action = intent.getAction();
        sp = context.getSharedPreferences(spNameString, context.MODE_PRIVATE);
        editor = sp.edit();
        if (ConnectionServiceManager.ACTION_CONNECTED_ADDRESS.equals(action)) {
            editor.putBoolean(keyName, true);
            Log.d("tt", "connect");
        } else if (ConnectionServiceManager.ACTION_DISCONNECTED_ADDRESS
                .equals(action)) {
            editor.putBoolean(keyName, false);
            Log.d("tt", "disconnect");
        }
        editor.commit();
    }

}
