///*
// *  Copyright (C) 2014 Ingenic Semiconductor
// *
// *  TangRongbing <rongbing.tang@ingenic.com, yanhuang8923@163.com>
// *
// *  This program is free software; you can redistribute it and/or modify it
// *  under the terms of the GNU General Public License as published by the
// *  Free Software Foundation; either version 2 of the License, or (at your
// *  option) any later version.
// *
// *  You should have received a copy of the GNU General Public License along
// *  with this program; if not, write to the Free Software Foundation, Inc.,
// *  675 Mass Ave, Cambridge, MA 02139, USA.
// *
// */
//
//package com.ingenic.remotecamera;
//
//import java.io.ByteArrayOutputStream;
//import java.util.Timer;
//import java.util.TimerTask;
//
//import android.annotation.SuppressLint;
//import android.content.DialogInterface;
//import android.content.DialogInterface.OnDismissListener;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Canvas;
//import android.graphics.Rect;
//import android.graphics.RectF;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.SurfaceHolder;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.ImageView;
//
//import com.ingenic.iwds.DeviceDescriptor;
//import com.ingenic.iwds.app.AmazingDialog;
//import com.ingenic.iwds.app.AmazingIndeterminateProgressDialog;
//import com.ingenic.iwds.app.RightScrollActivity;
//import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
//import com.ingenic.iwds.datatransactor.elf.CameraFrameInfo;
//import com.ingenic.iwds.datatransactor.elf.CameraPreviewSizeInfo;
//import com.ingenic.iwds.datatransactor.elf.CameraTransactionModel;
//import com.ingenic.iwds.widget.AmazingToast;
//import com.ingenic.iwds.widget.RightScrollView;
//import com.ingenic.remotecamera.RemoteCameraModel.ModelCallBack;
//
//public class AmazingRemoteCameraMainActivity2 extends RightScrollActivity
//        implements ModelCallBack {
//
//    private final String spNameString = "camera";
//    private final String keyName = "isLink";
//    private SharedPreferences sp;
//
//    // 旋转加载框
//    private AmazingIndeterminateProgressDialog mProgressDialog;
//
//    private RightScrollView mRightScrollView;
//    private ImageView mPreviewImageView;
//    private ImageView btnTakePicture;
//
//    private AmazingDialog mQuestDialog;
//    // 判断是否跳转到第二界面
//    private boolean isJump;
//
//    // 定时器
//    private Timer cameraTimer;
//    // 定时器时间
//    private final int setTime = 10000;
//
//    private final int MSG_FINISH_ACTIVITY = 0;
//    private final int MSG_UNAVAILABLE = 2;
//    private final int MSG_TAKEN_PICTURE_FAILED = 3;
//    private final int MSG_OPEN_CAMERA_FAILED = 4;
//
//    private boolean isPreviewing = false;
//    private boolean isClickedOk = false;
//    private boolean isToast = true;
//
//    private Bitmap prevBitmap = null;
//    private Bitmap curtBitmap = null;
//    private Bitmap mPicture = null;
//
//    @SuppressLint("HandlerLeak")
//    private final Handler mHandler = new Handler() {
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//            case MSG_FINISH_ACTIVITY:
//                isToast = false;
//                finish();
//                break;
//            case MSG_UNAVAILABLE:
//                if (mTransactionModel != null) {
//                    mTransactionModel.requestStopPreview();
//                }
//                if (isToast) {
//                    showToast(R.string.remote_camera_link_failed);
//                }
//                mHandler.obtainMessage(MSG_FINISH_ACTIVITY).sendToTarget();
//                break;
//            case MSG_TAKEN_PICTURE_FAILED:
//                showToast(R.string.take_picture_fail);
//                enableTakePicture();
//                break;
//            case MSG_OPEN_CAMERA_FAILED:
//                showToast(R.string.open_camera_fail_bt);
//                finish();
//                break;
//            default:
//                break;
//            }
//        };
//    };
//
//    private void clearOldBitmap() {
//        if (prevBitmap != null) {
//            prevBitmap.recycle();
//            prevBitmap = null;
//        }
//        prevBitmap = curtBitmap;
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        if (isJump) {
//            enableTakePicture();
//            mRightScrollView.enableRightScroll();
//            isJump = false;
//        }
//    }
//
//    private void recycleAll() {
//        if (prevBitmap != null) {
//            prevBitmap.recycle();
//            prevBitmap = null;
//        }
//        if (curtBitmap != null) {
//            curtBitmap.recycle();
//            curtBitmap = null;
//        }
//
//        if (mPicture != null) {
//            mPicture.recycle();
//            mPicture = null;
//        }
//
//    }
//
//    private void clearPrevBitmap() {
//        if (mPicture != null) {
//            mPicture.recycle();
//            mPicture = null;
//        }
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        sp = getSharedPreferences(spNameString, MODE_PRIVATE);
//
//        if (!sp.getBoolean(keyName, false)) {
//            showToast(R.string.link_disconnect);
//            mHandler.obtainMessage(MSG_FINISH_ACTIVITY).sendToTarget();
//            return;
//        }
//
//        if (mProgressDialog == null) {
//            mProgressDialog = new AmazingIndeterminateProgressDialog(this);
//            mProgressDialog.setCancelable(false);
//            // 显示对话框
//            mProgressDialog.show();
//        }
//
//        // 右滑相关
//        mRightScrollView = getRightScrollView();
//
//        mRightScrollView.disableRightScroll();
//
//        mRightScrollView.setContentView(R.layout.activity_camera_client);
//
//        // 预览图片
//        mPreviewImageView = (ImageView) findViewById(R.id.preview_frame_View);
//        // 拍照图片
//        btnTakePicture = (ImageView) findViewById(R.id.new_capture_imageview);
//        
//        btnTakePicture.setOnClickListener(takenPictureClickListener);
//
//        disableTakePicture();
//        // 构建拍照相关的事物层对象
//        startTransactionModel();
//
//        overTimeCloseDialog();
//    }
//
//    private OnClickListener takenPictureClickListener = new OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            // 通道可用情况下点击拍照按钮
//            if (mTransactionModel != null) {
//                mRightScrollView.disableRightScroll();
//                // 按钮不允许点击，停止预览
//                disableTakePicture();
//                // 请求拍照
//                mTransactionModel.requestTakePicture();
//            }
//        }
//    };
//
//    private void overTimeCloseDialog() {
//
//        if (cameraTimer != null) {
//            cameraTimer.cancel();
//            cameraTimer = null;
//        }
//        cameraTimer = new Timer();
//        cameraTimer.schedule(new TimerTask() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                if (mProgressDialog.isShowing()) {
//                    mHandler.obtainMessage(MSG_OPEN_CAMERA_FAILED)
//                            .sendToTarget();
//
//                }
//            }
//        }, setTime);
//    }
//
//    private void closeDialog() {
//        if (mProgressDialog != null && mProgressDialog.isShowing()) {
//            mProgressDialog.dismiss();
//        }
//    }
//
//    private void closeTimer() {
//        if (cameraTimer != null) {
//            cameraTimer.cancel();
//            cameraTimer = null;
//        }
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        isToast = false;
//        // 停止预览，关闭手机端预览界面
//        if (!isJump && mTransactionModel != null) {
//            mTransactionModel.requestStopPreview();
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        closeDialog();
//        closeTimer();
//        recycleAll();
//        isAvailable = false;
//        if (mTransactionModel != null) {
//            mTransactionModel.stop();
//            mTransactionModel = null;
//        }
//        super.onDestroy();
//    }
//
//    @Override
//    public void onRequestStartPreview(CameraPreviewSizeInfo sizeInfo) {
//    }
//
//    @Override
//    public void onRequestStopPreview() {
//        showToast(R.string.remote_camera_disconnect);
//        mHandler.obtainMessage(MSG_FINISH_ACTIVITY).sendToTarget();
//    }
//
//    @Override
//    public void onRequestTakePicture() {
//    }
//
//    @Override
//    public void onRequestStartPreviewFailed() {
//        mHandler.obtainMessage(MSG_FINISH_ACTIVITY).sendToTarget();
//    }
//
//    @Override
//    public void onRequestStopPreviewFailed() {
//        mHandler.obtainMessage(MSG_FINISH_ACTIVITY).sendToTarget();
//    }
//
//    @Override
//    public void onRequestTakePictureFailed() {
//        mHandler.obtainMessage(MSG_TAKEN_PICTURE_FAILED).sendToTarget();
//    }
//
//    @Override
//    public void onTakePictureDone(CameraFrameInfo frame) {
//        if (frame != null && frame.frameData != null) {
//            // 点击拍照按钮后的操作
//            if (!AmazingRemoteCameraMainActivity2.this.isFinishing()) {
//                clearPrevBitmap();
//                mPicture = BitmapFactory.decodeByteArray(frame.frameData, 0,
//                        frame.frameData.length);
//                getQuestDialog().show();
//            }
//        } else {
//            showToast(R.string.pic_transfer_failed);
//            enableTakePicture();
//        }
//    }
//
//    @Override
//    public void onObjectArrived(CameraFrameInfo frame) {
//        Log.d("ne", "watch object arrive");
//        if (isPreviewing) {
//            closeDialog();
//            mRightScrollView.enableRightScroll();
//            displayPreview(frame.frameData);
//            // 允许拍照，处于预览界面
//            enableTakePicture();
//        }
//    }
//
//    @Override
//    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
//    }
//
//    private boolean isAvailable = false;
//
//    @Override
//    public void onChannelAvailable(boolean isAvailable) {
//        this.isAvailable = isAvailable;
//        if (mTransactionModel != null && isAvailable) {
//            mTransactionModel.requestStartPreview(getCameraPreviewSizeInfo());
//        } else {
//            mHandler.obtainMessage(MSG_UNAVAILABLE).sendToTarget();
//        }
//    }
//
//    @Override
//    public void onSendResult(DataTransactResult result) {
//        int resultCode = result.getResultCode();
//        switch (resultCode) {
//        case DataTransactResult.RESULT_OK:
//            // 获取手表端发送的命令，告诉手机端正在处理数据
//
//            break;
//
//        case DataTransactResult.RESULT_FAILED_IWDS_CRASH:
//        case DataTransactResult.RESULT_FAILED_LINK_DISCONNECTED:
//
//            break;
//        case DataTransactResult.RESULT_FAILED_CHANNEL_UNAVAILABLE:
//            // 与手机端链接出现问题，远程音乐不可点击
//            break;
//
//        default:
//            break;
//        }
//    }
//
//    private void startTransactionModel() {
//        if (mTransactionModel == null) {
//            mTransactionModel = new CameraTransactionModel(
//                    AmazingRemoteCameraMainActivity2.this, this, PREVIEW_UUID);
//            mTransactionModel.start();
//            isPreviewing = true;
//        }
//    }
//
//    private CameraPreviewSizeInfo getCameraPreviewSizeInfo() {
//        CameraPreviewSizeInfo sizeInfo = new CameraPreviewSizeInfo();
//        int[] _pixels = getScreenPixels();
//        sizeInfo.width = _pixels[0];
//        sizeInfo.heigth = _pixels[1];
//        return sizeInfo;
//    }
//
//    private void displayPreview(byte[] pictureData) {
//        if (mPreviewImageView == null) {
//            return;
//        }
//        curtBitmap = BitmapFactory.decodeByteArray(pictureData, 0,
//                pictureData.length);
//        if (curtBitmap != null && !curtBitmap.isRecycled()) {
//            mPreviewImageView.setImageBitmap(curtBitmap);
//        }
//        clearOldBitmap();
//    }
//
//    private void disableTakePicture() {
//        // 按钮不允许点击以及不可见
//        btnTakePicture.setClickable(false);
//        btnTakePicture.setVisibility(View.GONE);
//        // 停止预览
//        isPreviewing = false;
//    }
//
//    // 允许拍照，处于预览界面
//    private void enableTakePicture() {
//        // 允许拍照
//        btnTakePicture.setClickable(true);
//        btnTakePicture.setVisibility(View.VISIBLE);
//        // 处于预览界面
//        isPreviewing = true;
//    }
//
//    private AmazingDialog getQuestDialog() {
//        if (mQuestDialog != null) {
//            return mQuestDialog;
//        }
//
//        mQuestDialog = new AmazingDialog(AmazingRemoteCameraMainActivity2.this);
//
//        mQuestDialog.setContent(getResources().getString(
//                R.string.browse_picture));
//
//        mQuestDialog.setNegativeButton(0, new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mQuestDialog.dismiss();
//                mRightScrollView.enableRightScroll();
//            }
//        });
//
//        mQuestDialog.setPositiveButton(0, new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (isClickedOk) {
//                    return;
//                }
//                isClickedOk = true;
//                mQuestDialog.dismiss();
//                showLookPictureDialog(mPicture);
//            }
//        });
//
//        mQuestDialog.setOnDismissListener(new OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
//                if (isClickedOk) {
//                    isClickedOk = false;
//                    return;
//                }
//                enableTakePicture();
//            }
//        });
//
//        return mQuestDialog;
//    }
//
//    private void showLookPictureDialog(Bitmap picture) {
//        if (picture == null) {
//            return;
//        }
//        isJump = true;
//        Intent intent = new Intent(AmazingRemoteCameraMainActivity2.this,
//                LookPictureActivity.class);
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        picture.compress(Bitmap.CompressFormat.PNG, 100, baos);
//        byte[] bitmapByte = baos.toByteArray();
//        intent.putExtra("bitmap", bitmapByte);
//        startActivity(intent);
//    }
//
//    public int screenWidth;
//    public int screenHeight;
//
//    public int[] getScreenPixels() {
//        int[] _pixels = new int[] { 0, 0 };
//
//        DisplayMetrics dm = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(dm);
//        screenWidth = _pixels[0] = dm.widthPixels;
//        screenHeight = _pixels[1] = dm.heightPixels;
//
//        return _pixels;
//    }
//
//    public void showToast(int resId) {
//        AmazingToast.showToast(this, resId, AmazingToast.LENGTH_SHORT);
//    }
//
//    public void setFrameToDisplay(byte[] data, SurfaceHolder holder, RectF rectf) {
//        if (data == null) {
//            return;
//        }
//
//        if (rectf == null) {
//            rectf = new RectF(0, 0, screenWidth, screenHeight);
//        }
//
//        byte[] _data = data;
//        Bitmap _bitmap = BitmapFactory.decodeByteArray(_data, 0, _data.length);
//        Canvas _canvas = holder.lockCanvas();
//
//        if (_canvas != null && _bitmap != null) {
//            Rect src = new Rect(0, 0, _bitmap.getWidth(), _bitmap.getHeight());
//            _canvas.drawBitmap(_bitmap, src, rectf, null);
//            holder.unlockCanvasAndPost(_canvas);
//        }
//
//    }
//
//}
