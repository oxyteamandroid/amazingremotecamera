/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  Elf/AmazingRemoteCamera project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.remotecamera;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;

public class RemoteCameraTransactorModel implements DataTransactorCallback {

    private Context mContext;
    private static RemoteCameraTransactorModel mInstance = null;

    private static DataTransactor m_transactor = null;

    public static final int MSG_WATCH_EXIT_CAMERA = 0x010;
    public static final int MSG_WATCT_LOCK_SCREEN = 0x011;

    private Handler mHandler = null;

    // 相机传输model
    public static String UUID = "635218be-c63a-11f4-af0f-000b2f597016";

    private RemoteCameraTransactorModel(Context context) {
        mContext = context;
        if (m_transactor == null) {
            m_transactor = new DataTransactor(mContext, this, UUID);
        }
    }

    public synchronized static RemoteCameraTransactorModel getInstance(
            Context context) {
        if (mInstance == null) {
            mInstance = new RemoteCameraTransactorModel(context);
        }
        return mInstance;
    }

    public void startTransaction(Handler handler) {

        mHandler = handler;
        if (m_transactor != null)
            m_transactor.start();
    }

    public void stopTransaction() {
        if (m_transactor != null)
            m_transactor.stop();

    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {

    }

    @Override
    public void onSendResult(DataTransactResult result) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDataArrived(Object object) {
        int flag = Integer.parseInt(object.toString());
        Log.e("test","=====on data arrived flag===="+flag);
        switch (flag) {
        case MSG_WATCH_EXIT_CAMERA:
            // 手机端主动点击返回或者HOME键推出手表端的应用
            mHandler.removeMessages(MSG_WATCH_EXIT_CAMERA);
            mHandler.obtainMessage(MSG_WATCH_EXIT_CAMERA).sendToTarget();

            break;
        case MSG_WATCT_LOCK_SCREEN:
            // 手机端没有解锁，导致打开相机失败
            mHandler.removeMessages(MSG_WATCT_LOCK_SCREEN);
            mHandler.obtainMessage(MSG_WATCT_LOCK_SCREEN).sendToTarget();
            break;

        default:
            break;
        }

    }

    @Override
    public void onSendFileProgress(int progress) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onRecvFileProgress(int progress) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSendFileInterrupted(int index) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onRecvFileInterrupted(int index) {
        // TODO Auto-generated method stub
    }

}
