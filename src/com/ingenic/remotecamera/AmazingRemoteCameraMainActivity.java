/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  Elf/AmazingRemoteCamera project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.remotecamera;

import java.io.ByteArrayOutputStream;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.app.AmazingIndeterminateProgressDialog;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.elf.CameraFrameInfo;
import com.ingenic.iwds.datatransactor.elf.CameraPreviewSizeInfo;
import com.ingenic.iwds.uniconnect.ConnectionServiceManager;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.iwds.widget.RightScrollView;

public class AmazingRemoteCameraMainActivity extends RightScrollActivity
        implements ServiceClient.ConnectionCallbacks {

    ServiceClient m_client;

    private RemoteCameraModel mRemoteCameraModel;

    private RemoteCameraTransactorModel mRemoteCameraTransactorModel;

    // 旋转加载框
    private AmazingIndeterminateProgressDialog mProgressDialog;

    private RightScrollView mRightScrollView;
    private ImageView mPreviewImageView;
    private ImageView btnTakePicture;

    private AmazingDialog mQuestDialog;
    // 判断是否跳转到第二界面
    private boolean isJump;

    // 延时
    private final int DELAY_TIME = 10000;

    public static final int MSG_MOBILE_PREVIEW_FAILED = -1;
    public static final int MSG_FINISH_ACTIVITY = 0;
    public static final int MSG_UNAVAILABLE = 2;
    public static final int MSG_TAKEN_PICTURE_FAILED = 3;
    public static final int MSG_OPEN_CAMERA_FAILED = 4;
    public static final int MSG_SERVICE_CONNECT = 5;
    public static final int MSG_SERVICE_DISCONNECT = 6;
    public static final int ON_TAKE_PICTURE_DONE = 7;
    public static final int ON_OBJECT_ARRIVED = 8;
    public static final int ON_CHANNEL_AVAILABLE = 9;
    public static final int ON_SEND_RESULT = 10;

    private boolean isPreviewing = true;
    private boolean isClickedOk = false;
    private boolean isToast = true;
    private boolean onSendResult = false;

    private Bitmap prevBitmap = null;
    private Bitmap curtBitmap = null;
    private Bitmap mPicture = null;
    private boolean mScreenisRound = false;

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case RemoteCameraTransactorModel.MSG_WATCH_EXIT_CAMERA:
                if (isToast) {
                    showToast(R.string.watch_close_camera_auto);
                }
                break;
            case RemoteCameraTransactorModel.MSG_WATCT_LOCK_SCREEN:
                if (isToast) {
                    showToast(R.string.watch_lock_tip);
                }
                break;
            case MSG_MOBILE_PREVIEW_FAILED:
                if (isToast) {
                    // showToast(R.string.open_camera_fail);
                }
                mHandler.removeMessages(AmazingRemoteCameraMainActivity.MSG_FINISH_ACTIVITY);
                mHandler.obtainMessage(
                        AmazingRemoteCameraMainActivity.MSG_FINISH_ACTIVITY)
                        .sendToTarget();
                break;
            case MSG_FINISH_ACTIVITY:
                isToast = false;
                finish();
                break;
            case MSG_UNAVAILABLE:

                if (mRemoteCameraModel != null)
                    mRemoteCameraModel.stopPreviewCamera();

                if (isToast) {
                    // showToast(R.string.open_camera_fail);
                }
                mHandler.obtainMessage(MSG_FINISH_ACTIVITY).sendToTarget();
                break;
            case MSG_TAKEN_PICTURE_FAILED:
                showToast(R.string.take_picture_fail);
                enableTakePicture();
                break;
            case MSG_OPEN_CAMERA_FAILED:
                // if (!mRemoteCameraModel.getAvaiableCamera()) {
                showToast(R.string.open_camera_fail);
                finish();
                // }
                break;
            case MSG_SERVICE_CONNECT:
                initView();
                break;
            case MSG_SERVICE_DISCONNECT:
                if (isToast) {
                    showToast(R.string.connect_none_device);
                }
                mHandler.obtainMessage(MSG_FINISH_ACTIVITY).sendToTarget();
                break;
            case ON_TAKE_PICTURE_DONE:
                CameraFrameInfo frame = mRemoteCameraModel.getCameraFrameInfo();

                if (frame != null && frame.frameData != null) {
                    // 点击拍照按钮后的操作
                    if (!AmazingRemoteCameraMainActivity.this.isFinishing()) {
                        clearPrevBitmap();
                        mPicture = BitmapFactory.decodeByteArray(
                                frame.frameData, 0, frame.frameData.length);
                        getQuestDialog().show();
                    }
                } else {
                    showToast(R.string.pic_transfer_failed);
                    enableTakePicture();
                }
                break;
            case ON_OBJECT_ARRIVED:
                CameraFrameInfo frameObj = mRemoteCameraModel
                        .getCameraFrameInfo();
                if (isPreviewing) {

                    displayPreview(frameObj.frameData);
                    mHandler.removeMessages(MSG_OPEN_CAMERA_FAILED);
                    // 允许拍照，处于预览界面
                    enableTakePicture();
                }
                break;
            case ON_CHANNEL_AVAILABLE:

                if (mRemoteCameraModel != null
                        && mRemoteCameraModel.getAvaiableCamera()) {

                    mRemoteCameraModel
                            .mRequestStartPreview(getCameraPreviewSizeInfo());
                } else {
                    mHandler.obtainMessage(MSG_UNAVAILABLE).sendToTarget();
                }

                break;
            case ON_SEND_RESULT:

                onSendResult = true;
                if (mRightScrollView != null)
                    mRightScrollView.enableRightScroll();
                break;

            default:
                break;
            }
        };
    };

    private void initView() {

        mRemoteCameraModel = RemoteCameraModel.getInstance(this);
        mRemoteCameraModel.startTransaction(mHandler);

        mRemoteCameraTransactorModel = RemoteCameraTransactorModel
                .getInstance(this);
        mRemoteCameraTransactorModel.startTransaction(mHandler);

        // 右滑相关
        mRightScrollView = getRightScrollView();

        mRightScrollView.disableRightScroll();
        mScreenisRound = Utils.isCircularScreen();
        if (mScreenisRound) {
            mRightScrollView.setContentView(R.layout.activity_camera_client_round);
            mRightScrollView.setBackgroundResource(R.drawable.round);
        } else {
            mRightScrollView.setContentView(R.layout.activity_camera_client);
        }
        // 预览图片
        mPreviewImageView = (ImageView) findViewById(R.id.preview_frame_View);
        // 拍照图片
        btnTakePicture = (ImageView) findViewById(R.id.new_capture_imageview);

        btnTakePicture.setOnClickListener(takenPictureClickListener);

        overTimeCloseDialog();

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (isJump) {
            enableTakePicture();
            if (mRightScrollView != null)
                mRightScrollView.enableRightScroll();
            isJump = false;
        }

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        if (onSendResult) {
            if (mRightScrollView != null) {
                mRightScrollView.enableRightScroll();
                onSendResult = false;
            }
        }

    }

    private void recycleAll() {
        if (prevBitmap != null && !prevBitmap.isRecycled()) {
            prevBitmap.recycle();
            prevBitmap = null;
        }
        if (curtBitmap != null && !curtBitmap.isRecycled()) {
            curtBitmap.recycle();
            curtBitmap = null;
        }

        if (mPicture != null && !mPicture.isRecycled()) {
            mPicture.recycle();
            mPicture = null;
        }

    }

    private void clearPrevBitmap() {
        if (mPicture != null && !mPicture.isRecycled()) {
            mPicture.recycle();
            mPicture = null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mProgressDialog == null) {
            mProgressDialog = new AmazingIndeterminateProgressDialog(this);
            mProgressDialog.setCancelable(false);
            // 显示对话框
            mProgressDialog.show();
        }
        m_client = new ServiceClient(this,
                ServiceManagerContext.SERVICE_CONNECTION, this);
        m_client.connect();
    }

    private OnClickListener takenPictureClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // 通道可用情况下点击拍照按钮
            if (mRemoteCameraModel != null
                    && mRemoteCameraModel.getAvaiableCamera()) {
                if (mRightScrollView != null)
                    mRightScrollView.disableRightScroll();

                // 按钮不允许点击，停止预览
                disableTakePicture();
                // 请求拍照
                mRemoteCameraModel.requestTakePictrue();
            }
        }
    };

    private void overTimeCloseDialog() {
        // TODO Auto-generated method stub
        if (mProgressDialog.isShowing()) {
            mHandler.removeMessages(MSG_OPEN_CAMERA_FAILED);
            mHandler.sendEmptyMessageDelayed(MSG_OPEN_CAMERA_FAILED, DELAY_TIME);
        }
    }

    private void closeDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isToast = false;
        mHandler.removeMessages(MSG_OPEN_CAMERA_FAILED);
        // 停止预览，关闭手机端预览界面
        if (!isJump && mRemoteCameraModel != null) {
            mRemoteCameraModel.stopPreviewCamera();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearOldBitmap();
        mHandler.removeMessages(MSG_OPEN_CAMERA_FAILED);
        if (mRemoteCameraModel != null)
            mRemoteCameraModel.stopTransaction();

        if (mRemoteCameraTransactorModel != null)
            mRemoteCameraTransactorModel.stopTransaction();

        closeDialog();
        recycleAll();

    }

    private CameraPreviewSizeInfo getCameraPreviewSizeInfo() {
        CameraPreviewSizeInfo sizeInfo = new CameraPreviewSizeInfo();
        int[] _pixels = getScreenPixels();
        sizeInfo.width = _pixels[0];
        sizeInfo.heigth = _pixels[1];
        return sizeInfo;
    }

    private void clearOldBitmap() {
        if (prevBitmap != null && !prevBitmap.isRecycled()) {
            prevBitmap.recycle();
            prevBitmap = null;
        }
        prevBitmap = curtBitmap;
    }

    private void displayPreview(byte[] pictureData) {
        if (mPreviewImageView == null) {
            return;
        }
        curtBitmap = BitmapFactory.decodeByteArray(pictureData, 0,
                pictureData.length);
        if (curtBitmap != null && !curtBitmap.isRecycled()) {
            mPreviewImageView.setImageBitmap(curtBitmap);
        }

        closeDialog();

        clearOldBitmap();
    }

    private void disableTakePicture() {
        // 按钮不允许点击以及不可见
        btnTakePicture.setClickable(false);
        btnTakePicture.setVisibility(View.GONE);

        // 停止预览
        isPreviewing = false;
    }

    // 允许拍照，处于预览界面
    private void enableTakePicture() {
        // 允许拍照
        btnTakePicture.setClickable(true);
        btnTakePicture.setVisibility(View.VISIBLE);
        // 处于预览界面
        isPreviewing = true;
    }

    private AmazingDialog getQuestDialog() {
        if (mQuestDialog != null) {
            return mQuestDialog;
        }

        mQuestDialog = new AmazingDialog(AmazingRemoteCameraMainActivity.this);

        mQuestDialog.setContent(getResources().getString(
                R.string.browse_picture));

        mQuestDialog.setNegativeButton(0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mQuestDialog.dismiss();
                if (mRightScrollView != null)
                    mRightScrollView.enableRightScroll();
            }
        });

        mQuestDialog.setPositiveButton(0, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClickedOk) {
                    return;
                }
                isClickedOk = true;
                mQuestDialog.dismiss();
                showLookPictureDialog(mPicture);
            }
        });

        mQuestDialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (isClickedOk) {
                    isClickedOk = false;
                    return;
                }
                enableTakePicture();
            }
        });

        return mQuestDialog;
    }

    private void showLookPictureDialog(Bitmap picture) {
        if (picture == null) {
            return;
        }
        isJump = true;
        Intent intent = new Intent(AmazingRemoteCameraMainActivity.this,
                LookPictureActivity.class);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        picture.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] bitmapByte = baos.toByteArray();
        intent.putExtra("bitmap", bitmapByte);
        startActivity(intent);
    }

    public int screenWidth;
    public int screenHeight;

    public int[] getScreenPixels() {
        int[] _pixels = new int[] { 0, 0 };

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        screenWidth = _pixels[0] = dm.widthPixels;
        screenHeight = _pixels[1] = dm.heightPixels;

        return _pixels;
    }

    public void showToast(int resId) {
        AmazingToast.showToast(this, resId, AmazingToast.LENGTH_SHORT);
    }

    public void setFrameToDisplay(byte[] data, SurfaceHolder holder, RectF rectf) {
        if (data == null) {
            return;
        }

        if (rectf == null) {
            rectf = new RectF(0, 0, screenWidth, screenHeight);
        }

        byte[] _data = data;
        Bitmap _bitmap = BitmapFactory.decodeByteArray(_data, 0, _data.length);
        Canvas _canvas = holder.lockCanvas();

        if (_canvas != null && _bitmap != null) {
            Rect src = new Rect(0, 0, _bitmap.getWidth(), _bitmap.getHeight());
            _canvas.drawBitmap(_bitmap, src, rectf, null);
            holder.unlockCanvasAndPost(_canvas);
        }

    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        ConnectionServiceManager manager = (ConnectionServiceManager) serviceClient
                .getServiceManagerContext();
        if (manager.getConnectedDeviceDescriptors().length != 0) {
            // Link connected
            mHandler.obtainMessage(MSG_SERVICE_CONNECT).sendToTarget();

        } else {
            mHandler.obtainMessage(MSG_SERVICE_DISCONNECT).sendToTarget();
        }
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {

    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {
        // TODO Auto-generated method stub

    }

}
