/*
 *  Copyright (C) 2014 Ingenic Semiconductor
 *
 *  ShiGuanghua(kenny) <guanghua.shi@ingenic.com>
 *
 *  Elf/AmazingRemoteCamera project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.remotecamera;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.CameraFrameInfo;
import com.ingenic.iwds.datatransactor.elf.CameraPreviewSizeInfo;
import com.ingenic.iwds.datatransactor.elf.CameraTransactionModel;
import com.ingenic.iwds.datatransactor.elf.CameraTransactionModel.CameraTransactionModelCallback;

public class RemoteCameraModel implements CameraTransactionModelCallback {

    private Context mContext;
    private static RemoteCameraModel mInstance = null;
    private static CameraTransactionModel mCameraTransactionModel = null;

    public static final String PREVIEW_UUID = "08BDB878-8A44-9C6A-8542-C543B833C681";

    private static boolean mAvaiableCamera = false;

    private CameraFrameInfo mCameraFrameInfo = null;

    private Handler mHandler = null;

    private RemoteCameraModel(Context context) {
        mContext = context;
        if (mCameraTransactionModel == null) {
            mCameraTransactionModel = new CameraTransactionModel(mContext,
                    this, PREVIEW_UUID);
        }

    }

    public synchronized static RemoteCameraModel getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RemoteCameraModel(context);
        }

        return mInstance;
    }

    public void startTransaction(Handler handler) {
        mHandler = handler;

        if (mCameraTransactionModel != null)
            mCameraTransactionModel.start();
    }

    public void mRequestStartPreview(
            CameraPreviewSizeInfo mCameraPreviewSizeInfo) {
        if (mCameraTransactionModel != null && mAvaiableCamera) {
            mCameraTransactionModel.requestStartPreview(mCameraPreviewSizeInfo);
        }
    }

    public void stopPreviewCamera() {

        if (mCameraTransactionModel != null) {

            mCameraTransactionModel.requestStopPreview();

        }

    }

    public void stopTransaction() {

        if (mCameraTransactionModel != null) {

            mCameraTransactionModel.stop();

        }

    }

    public void requestTakePictrue() {

        if (mCameraTransactionModel != null) {

            mCameraTransactionModel.requestTakePicture();

        }

    }

    public boolean getAvaiableCamera() {
        return mAvaiableCamera;
    }

    @Override
    public void onRequestStartPreview(CameraPreviewSizeInfo sizeInfo) {

    }

    @Override
    public void onRequestStopPreview() {

        mHandler.removeMessages(AmazingRemoteCameraMainActivity.MSG_MOBILE_PREVIEW_FAILED);
        mHandler.obtainMessage(
                AmazingRemoteCameraMainActivity.MSG_MOBILE_PREVIEW_FAILED)
                .sendToTarget();

    }

    @Override
    public void onRequestTakePicture() {

    }

    @Override
    public void onRequestStartPreviewFailed() {
        mHandler.removeMessages(AmazingRemoteCameraMainActivity.MSG_FINISH_ACTIVITY);
        mHandler.obtainMessage(
                AmazingRemoteCameraMainActivity.MSG_FINISH_ACTIVITY)
                .sendToTarget();

    }

    @Override
    public void onRequestStopPreviewFailed() {
        mHandler.removeMessages(AmazingRemoteCameraMainActivity.MSG_FINISH_ACTIVITY);
        mHandler.obtainMessage(
                AmazingRemoteCameraMainActivity.MSG_FINISH_ACTIVITY)
                .sendToTarget();

    }

    @Override
    public void onRequestTakePictureFailed() {
        mHandler.removeMessages(AmazingRemoteCameraMainActivity.MSG_TAKEN_PICTURE_FAILED);
        mHandler.obtainMessage(
                AmazingRemoteCameraMainActivity.MSG_TAKEN_PICTURE_FAILED)
                .sendToTarget();

    }

    @Override
    public void onTakePictureDone(CameraFrameInfo frame) {
        mCameraFrameInfo = frame;
        Log.e("test","=====on take picture done");

        mHandler.obtainMessage(
                AmazingRemoteCameraMainActivity.ON_TAKE_PICTURE_DONE)
                .sendToTarget();

    }

    public CameraFrameInfo getCameraFrameInfo() {
        return mCameraFrameInfo;
    }

    @Override
    public void onObjectArrived(CameraFrameInfo frame) {
        mCameraFrameInfo = frame;
        Log.e("test","==remote camera====on object arrived=====");
        mHandler.obtainMessage(
                AmazingRemoteCameraMainActivity.ON_OBJECT_ARRIVED)
                .sendToTarget();

    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        if (isAvailable) {
            mAvaiableCamera = true;
        } else {
            mAvaiableCamera = false;
        }

        mHandler.obtainMessage(
                AmazingRemoteCameraMainActivity.ON_CHANNEL_AVAILABLE)
                .sendToTarget();

    }

    @Override
    public void onSendResult(DataTransactResult result) {

        switch (result.getResultCode()) {
        case DataTransactResult.RESULT_OK:
            break;

        case DataTransactResult.RESULT_FAILED_CHANNEL_UNAVAILABLE:
            break;

        case DataTransactResult.RESULT_FAILED_LINK_DISCONNECTED:
            break;

        default:
            break;
        }

        mHandler.obtainMessage(AmazingRemoteCameraMainActivity.ON_SEND_RESULT)
                .sendToTarget();

    }

}
